# Ren'Py reverse-engineering tools

These are very simplistic and probably won't work properly without some editing
for any given version of Ren'py.

## Usage

### rpa.py

    rpa.py ../archive.rpa

Extracts resource (`.rpa`) archives into your current working directory,
printing a list of files while it does so.
You'll probably want to run it in a clean directory for simplicity.

### rpy.py

    rpy.py ../screen.rpyc > screen.rpy

Decompiles `screen.rpyc` to stdout. Recommend redirecting to a file for easier
browsing. Because the pickled representation has hooks into all kinds of renpy
objects, many scripts will probably require some amount of environment setup
at the beginning of `rpy.py`.

## License
Copyright (c) 2014 Peter Marheine

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

The Software shall be used for Good, not Evil.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 
