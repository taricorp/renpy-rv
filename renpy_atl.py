class RawStatement(object):
    def decompile(self, formatter):
        formatter.print(str(type(self)))

class RawBlock(RawStatement):
    """
    Properties:
     * statements: list of RawStatement
     * animation: bool flagging which timebase to use
    """
    # TODO does the animation property mean anything useful?
    def decompile(self, formatter):
        for stmt in self.statements:
            stmt.decompile(formatter)

class RawMultipurpose(RawStatement):
    pass

class RawParallel(RawStatement):
    __slots__ = ('block',)

    def decompile(self, formatter):
        pass

class RawRepeat(RawStatement):
    pass

class RawOn(RawStatement):
    pass
