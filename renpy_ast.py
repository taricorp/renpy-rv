# Of __slots__ and _pickle
# 
# The __slots__ class attribute permits memory-efficient objects by suppressing
# the default __dict__ on objects (unless necessary).
#
# With _pickle (the C pickle implementation), protocol 2 encodes the state
# of each item (per __getstate__) as a tuple of (__dict__, __slots__). Our
# class definitions must match the slots declared by Ren'Py, otherwise
# _pickle freaks out when __dict__ is None.
#
# When __slots__ is defined, __setstate__ appears to receive a tuple of slot
# values. As such, our defined slots must exactly match those of the original.
import ast as py_ast
from functools import reduce

class Node(object):
    __slots__ = (
        'name',
        'filename',
        'linenumber',
        'next'
    )

    """Base AST node."""
    def __init__(self, loc):
        self.filename, self.linenumber = loc
        self.name = self.next = None

    def __setstate__(self, state):
        for k, v in state[1].items():
            try:
                setattr(self, k, v)
            except AttributeError:
                pass

    def decompile(self, formatter):
        formatter.print(type(self))

class Init(Node):
    """Code executed at engine startup."""
    __slots__ = (
        # [Node]
        'block',
        # int, Lower numbers execute first
        'priority'
    )

    def decompile(self, formatter):
        with formatter.block('init {}:', self.priority):
            for item in self.block:
                item.decompile(formatter)

class ParameterInfo(object):
    """
    Fields:
    parameters: list of (name, default) tuples for all non-* parameters.
    positional: list of names of positional-only parameters (not kwargs)
    extrapos: name of variable capturing extra positional arguments (*args)
    extrakw: like extrapos for keyword arguments (**kwargs)
    """
    def __str__(self):
        s = []
        parameters = dict(self.parameters)
        for p in self.positional:
            s.append(p)
            del parameters[p]
        for (name, value) in self.parameters.items():
            s.append('{}={}'.format(name, value))

        if self.extrapos:
            s.append('*{}'.format(self.extrapos))
        if self.extrakw:
            s.append('**{}'.format(self.extrakw))

        return ', '.join(s)


class Transform(Node):
    """
    ATL transform.

    The transform described by the combination of `atl` and `parameters` is
    exported in the engine store as `varname`.
    """
    __slots__ = (
        'varname',      # str
        'atl',          # Atl subclass
        'parameters'    # ParameterInfo or None
    )

    def decompile(self, formatter):
        with formatter.block('transform {}({}):', self.varname,
                             self.parameters or ''):
            self.atl.decompile(formatter)


class PyExpr(str):
    """A string containing Python code."""
    __slots__ = (
        'filename',
        'linenumber'
    )

    def __new__(cls, s, filename, linenumber):
        self = str.__new__(cls, s)
        self.filename = filename
        self.linenumber = linenumber
        return self

    def __getnewargs__(self):
        """Returns the expected parameters to __new__ when unpickling."""
        return (str(self), self.filename, self.linenumber)


class PyCode(object):
    """
    Raw python code.

    Properties:
     * source: PyExpr or native python AST
     * location: triple of (filename, linenumber, build_time), or quadruple
       of (filename, linenumber, PyCode, build_time)
     * mode: 'eval' or 'exec'

    Unimportant properties:
     * hash: renpy.bytecode_version + MD5(location + code as UTF-8)
     * bytecode: compiled bytecode for source
    """
    __slots__ = (
        'source',
        'location',
        'mode',
        'bytecode',
        'hash',
    )

    def __getstate__(self):
        """Pickled representation deliberately excludes bytecode."""
        return (1, self.source, self.location, self.mode)

    def __setstate__(self, state):
        (_, self.source, self.location, self.mode) = state
        self.bytecode = None

    def __str__(self):
        return self.source

    def decompile(self, formatter):
        if isinstance(self.source, str):
            # Source code. Easy.
            for line in self.source:
                formatter.print(line)
        else:
            # Python native AST. Not so easy.
            formatter.print(py_ast.dump(self.source, annotate_fields=False))


class Python(Node):
    """
    Block of Python code.

    `hide` determines side-effect availability during execution. If True, the
    code is executed with its own set of locals. `store` appears to determine
    the named environment to execute in (the engine maintains user-specified
    environments and a default 'store').
    """
    def __init__(self, loc, code, hide=False, store='store'):
        super().__init__(loc)
        self.hide = hide
        self.code = PyCode(code, loc=loc, mode='exec')
        self.store = store

    def get_options(self):
        p = []
        if self.hide:
            p.append('hide')
        if self.store != 'store':
            p.extend(('in', self.store))
        return p

    def decompile(self, formatter):
        code = str(self.code).split('\n')
        options = ' '.join(self.get_options())

        if options or len(code) > 1:
            if options:
                options = ' ' + options
            with formatter.block('python{}:', options):
                for line in code:
                    formatter.print(line)
        else:
            # Can use the single-line notation
            formatter.print('$ {}', code[0])

class EarlyPython(Python):
    """
    Not sure what the difference between `python` and `python early` is
    in the interpreter.
    """
    def get_options(self):
        return ['early'] + super().get_options()

class Image(Node):
    """
    Defines an displayable image.

    Only one of `code` or `atl` should be given; the result of executing the
    PyCode (`expr`) or ATL is exported as `name`in the environment.
    """
    __slots__ = (
        'imgname',
        'code',
        'atl'
    )

    def decompile(self, formatter):
        name = ' '.join(self.imgname)
        if self.code:
            # Python variant
            formatter.print('image {} = {}', name, self.code)
        else:
            # ATL variant
            with formatter.block('image {}:', name):
                self.atl.decompile(formatter)

class Label(Node):
    __slots__ = (
        'name',
        'parameters',   # not present in source
        'block'
    )

    def __setstate__(self, state):
        self.parameters = None
        super().__setstate__(state)

class Scene(Node):
    __slots__ = (
        'imspec',   # ((str, str, str), [at expression], optional layer)
        'layer',    # image to display if layer in imspec is not None?
        'atl'       # atl.RawBlock
    )

class With(Node):
    __slots__ = (
        'expr',
        'paired'    # Not present in source
    )

    def __setstate__(self, state):
        self.paired = None
        super().__setstate__(state)

class UserStatement(Node):
    __slots__ = (
        'line',
        'parsed',
        'block'
    )

    def __setstate__(self, state):
        self.block = []
        super().__setstate__(state)

class Jump(Node):
    """Jumps to label `target` if `expression` evaluates to True."""
    __slots__ = (
        'target',
        'expression'
    )

class Screen(Node):
    """screen foo(parameters) [keyword..]: screendef"""
    __slots__ = (
        'screen',
    )

    def decompile(self, formatter):
        self.screen.decompile(formatter)

class ScreenLangScreen(object):
    """
    renpy.screenlang.ScreenLangScreen

    Screen definitions have their own parser (renpy.screenlang); there are
    a lot of options here, and it's actually defined as its own DSL within
    renpy.

    Properites:
    modal: simple_expression
    zorder: simple_expression
    tag: word
    variant: simple_expression
    code: PyCode, translated from screen language

    All properties except `code` are taken directly from the optional
    keywords.
    """
    def decompile(self, formatter):
        parameters = self.parameters
        options = '<screen options>'
        with formatter.block('screen {}({}){}:',
                             self.name,
                             parameters,
                             options):
            self.code.decompile(formatter)

class If(Node):
    __slots__ = (
        'entries',  # [(PyExpr, Block)], condition & corresponding code
    )

class Pass(Node):
    def decompile(self, formatter):
        formatter.print('pass')

class Show(Node):
    __slots__ = (
        'imspec',
        'atl'
    )

class Hide(Node):
    __slots__ = (
        'imspec',
    )

class Return(Node):
    __slots__ = (
        'expression',
    )

# TODO don't think this is necessary
#    def __setstate__(self, state):
#        self.expression = None
#        super().__setstate__(self, state)
